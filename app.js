'use strict';
const form = document.querySelector('form');
const eField = form.querySelector('.email');
const pField = form.querySelector('.password');
const eInput = form.querySelector('.email input');
const pInput = form.querySelector('.password input');
let errorMap = new Map();
errorMap.set('Email',true);
errorMap.set('Password',true);

// submit checked

form.addEventListener('submit',(event) =>{
    event.preventDefault();
    if(errorMap.get('Email')){
        eField.classList.add('errorInput','shake');
    }
    if(errorMap.get('Password')){
        pField.classList.add('errorInput','shake');
    }

    setTimeout(()=>{
        eField.classList.remove('shake');
        pField.classList.remove('shake');
    },500);

    //Submitted Form
    let totalTrueMap = 0;
    let totalItemMap = 0;
    for(let e of errorMap.values()){
        totalItemMap += 1;
        if(!e){
            totalTrueMap += 1;
        }
    }
    if(totalItemMap == totalTrueMap){
        form.submit();
    }
    

})

form.addEventListener('input',(event) =>{
    if(event.target.closest('.email')){
        eField.classList.remove('errorInput');
        errorMap.set('Email',false);

        let pattern = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;

        const errorMessageObj = document.querySelector('.email .error-text');

        errorMessageObj.textContent = (event.target.value == "") ? "Email can't be blank" : "Enter a valid email address";

        if(!(event.target.value.match(pattern))){
            eField.classList.add('errorInput');
            errorMap.set('Email',true);
        }

    }else if(event.target.closest('.password')){
        
        pField.classList.remove('errorInput');
        errorMap.set('Password',false);
    
        if(pInput.value == ''){
            pField.classList.add('errorInput');
            errorMap.set('Password',true);
        }
    }
    
})

